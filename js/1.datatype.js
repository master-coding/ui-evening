var i = 10;
console.log("10 is of type " + typeof i);

var j = 10.23;
console.log("10.23 is of type " + typeof j);

var k = true;
console.log("true is o type " + typeof k);

var l = "javascript";
console.log("javascript is of type ", typeof l);

var m;
console.log("uninitialized variable is of type ", typeof m);

var n = [1, 2, 3];
console.log("[1,2,3] of type ", typeof n);

var o = {
    "name": "swagat",
    "area": "nagole"
};
console.log("javascript object is of type ", typeof o);

var p = null;
console.log("null is of type", typeof p);

//var, let, const

var q = 2;
var q = 20;

console.log("redefined q using var is ", q);
//always prefer let over var in js
let q1 = 3;
//let q1 = 30;
console.log("redfiend q1 using let is an error");

const r = 20;
// r = 40;
console.log("The value of const is ", r);
