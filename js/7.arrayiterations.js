let numbers = [10, 5, 20, 7, 30, 5, 40];

for(let i=0; i < numbers.length; i++){
    console.log(numbers[i]);
}

console.log(typeof numbers);
console.log("-----------------------------------------------");
numbers.forEach(function (value) {
    console.log(value);
});

//get the array which does square of every element in an array.
let squares = numbers.map(function (value) {
   return value * value;
});

console.log(squares);

//return the array which contains the elements greater than or equals to 10.
let filterArray = numbers.filter(function (value) {
   if( value >= 10){
       return true;
   }else{
       return false;
   }
});

console.log(filterArray);

//find the sum of all the elements
let sum = numbers.reduce(function (result, value) {
    return result + value;
});

console.log(sum);