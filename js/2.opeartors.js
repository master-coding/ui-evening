//Equality: == vs ===
//Not Equal: != vs !==

let i = 10;
let j = 10;

if(i == j){
    console.log("10 == 10 are : equal");
}else{
    console.log("10 == 10 are : not equal");
}

if(i === j){
    console.log("10 === 10 are : equal");
}else{
    console.log("10 === 10 are : not equal");
}

let i1 = 10;
let j1 = "10";

//checks for same value.
if(i1 == j1){
    console.log("10 == \"10\" are : equal");
}else{
    console.log("10 == \"10\" are : not equal");
}

//checks for same datatype and same value.
if(i1 === j1){
    console.log("10 === \"10\" are : equal");
}else{
    console.log("10 === \"10\" are : not equal");
}

let i3 = 10;
let j3 = "10";
if( i3 != j3){
    console.log("10 != \"10\" are : not equal");
}else{
    console.log("10 != \"10\" are : equal");
}

//is data type of i3 is not equal to j3
//is value of i3 is not equal to j3
if( i3 !== j3){
    console.log("10 !== \"10\" are : not equal");
}else{
    console.log("10 !== \"10\" are : equal");
}