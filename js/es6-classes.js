class Account {
    constructor(amount){
        this.balance = amount;
    }

    deposit(amount){
        this.balance = this.balance + amount;
        Account.logTransaction();
    }

    withdraw(amount){
        this.balance = this.balance - amount;
        Account.logTransaction();
    }

    static logTransaction(){
        Account.count++;
    }
}

Account.count = 0;

let ramAcc = new Account(1000);
ramAcc.deposit(400);
ramAcc.withdraw(100);

let lakAcc = new Account(500);
lakAcc.deposit(1000);
lakAcc.withdraw(300);

console.log(`The ram balance = ${ramAcc.balance}`);
console.log(`The lak balance = ${lakAcc.balance}`);
console.log(`The number of transactions = ${Account.count}`);

class SavingAccount extends Account{
    deposit(amount){
        super.deposit(amount + 2);
    }
}

let bharathAcc = new SavingAccount(300);
bharathAcc.deposit(500);
bharathAcc.withdraw(100);

console.log(`Bharath account balance = ${bharathAcc.balance}`);