function max(a, b) {
    return a > b ? a : b;
}

console.log(max(10, 30));
console.log(max(10.5, 20.4));
console.log(max("RAM", "LAK"));

function test(m) {
    console.log(m(10, 20));
}

test(max);

test(function (a, b) {
    return a+b;
});

let sub = function (a, b) {
    return a - b;
};

test(sub);

function max1() {
    return function (a, b) {
        return a > b ? a : b;
    }
}

let m1 = max1();
console.log(m1(100, 200));
console.log(max1()(100, 200));
