let i = 10;
if( i > 0){
    //string interpolation
    console.log(`${i} is positive`);
    console.log(i + " is positive");
}

i = -5;
if( i > 0){
    console.log(`${i} is positive`);
}else{
    console.log(`${i} is negative`);
}

let sum = 0;
i = 0;
while(i <= 5){
    sum = sum + i;
    i++;
}
console.log(`The sum of first first five numbers is ${sum}`);

sum = 0;
i = 0;
do {
    sum = sum + i;
    i++;
}while (i <= 5);
console.log(`The sum of first first five numbers is ${sum}`);

for(i=0, sum=0; i <=5; i++){
    sum = sum + i;
}
console.log(`The sum of first first five numbers is ${sum}`);

let weekday = 3;
switch (weekday) {
    case 1: console.log("MON"); break;
    case 2: console.log("TUE"); break;
    case 3: console.log("WED"); break;
    case 4: console.log("THU"); break;
    case 5: console.log("FRI"); break;
    case 6: console.log("SAT"); break;
    case 7: console.log("SUN"); break;
}

try {
    throw "some error";
}catch (e) {
    console.log(e);
}
