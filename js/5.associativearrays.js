// similar to hash map in java. key value pair

let phoneNumbers = {};
phoneNumbers[900] = "RAM";
phoneNumbers[901] = "LAK";
phoneNumbers[902] = "BHARATH";
phoneNumbers[903] = "JOHN";

console.log(typeof phoneNumbers);
console.log(phoneNumbers[902]);

phoneNumbers[903] = "JOHN1";

delete phoneNumbers[902];
console.log(phoneNumbers);